/** [powerBy]**/
package com.stylefeng.guns.modular.[model].controller;

import com.stylefeng.guns.common.annotion.log.BussinessLog;
import com.stylefeng.guns.common.constant.Dict;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.controller.BaseController;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.persistence.[model].model.[entityClass];
import com.stylefeng.guns.modular.[model].service.I[entityClass]Service;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.stylefeng.guns.modular.system.warpper.DeptWarpper;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * [description]控制器
 * [email]
 * @author zscat 951449465
 * @Date [date]
 */
@Controller
@RequestMapping("/[lowerentity]")
public class [entityClass]Controller extends BaseController {

    private String PREFIX = "/[model]/[lowerentity]/";

    @Resource
    private   I[entityClass]Service [entityClass]Service;

    /**
     * 跳转到[description]列表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "[lowerentity].html";
    }

    /**
     * 跳转到添加[description]
     */
    @RequestMapping("/[lowerentity]_add")
    public String [lowerentity]Add() {
        return PREFIX + "[lowerentity]_add.html";
    }

    /**
     * 跳转到修改[description]
     */
    @RequestMapping("/[lowerentity]_update/{[lowerentity]Id}")
    public String [lowerentity]Update(@PathVariable Integer [lowerentity]Id, Model model) {
        [entityClass] [lowerentity] = [entityClass]Service.selectById([lowerentity]Id);
        model.addAttribute("[lowerentity]",[lowerentity]);
        return PREFIX + "[lowerentity]_edit.html";
    }



    /**
     * 获取[description]列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
          List<Map<String, Object>> list = [entityClass]Service.selectMaps(null);
         return super.warpObject(new DeptWarpper(list));
    }

    /**
     * 新增[description]
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add([entityClass] [lowerentity]) {
        [entityClass]Service.insert([lowerentity]);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除[description]
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Long id) {
        [entityClass]Service.deleteById(id);
        return SUCCESS_TIP;
    }

    /**
     * 修改[description]
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update([entityClass] [lowerentity]) {
        [entityClass]Service.updateById([lowerentity]);
        return super.SUCCESS_TIP;
    }

}
