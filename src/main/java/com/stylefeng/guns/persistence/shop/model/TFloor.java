package com.stylefeng.guns.persistence.shop.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商品类型表
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@TableName("t_floor")
public class TFloor extends Model<TFloor> {

    private static final long serialVersionUID = 1L;

    /**
     * 类型id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 类型名称
     */
	private String name;
    /**
     * 商品类型排序
     */
	private Integer typesort;
    /**
     * 父id
     */
	@TableField("parent_id")
	private Long parentId;
	@TableField("parent_ids")
	private String parentIds;
	private String title;
	@TableField("del_flag")
	private String delFlag;
	private String advimg;
	private String advurl;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTypesort() {
		return typesort;
	}

	public void setTypesort(Integer typesort) {
		this.typesort = typesort;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public String getAdvimg() {
		return advimg;
	}

	public void setAdvimg(String advimg) {
		this.advimg = advimg;
	}

	public String getAdvurl() {
		return advurl;
	}

	public void setAdvurl(String advurl) {
		this.advurl = advurl;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
