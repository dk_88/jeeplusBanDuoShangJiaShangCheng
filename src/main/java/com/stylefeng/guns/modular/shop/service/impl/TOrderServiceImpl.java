package com.stylefeng.guns.modular.shop.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.util.RandomString;
import com.stylefeng.guns.persistence.shop.dao.*;
import com.stylefeng.guns.persistence.shop.model.*;
import com.stylefeng.guns.modular.shop.service.ITOrderService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.web.utils.MemberUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements ITOrderService {

    @Resource
    private TOrderMapper OrderMapper;
    @Resource
    private TGoodsMapper goodsMapper;
    @Resource
    private TGoodSorderMapper GoodsOrderMapper;
    @Resource
    private TCartMapper CartMapper;
    @Resource
    private TOrderLogMapper OrderLogMapper;
    @Override
    public TOrder insertOrder(String[] cartIds,Long addressid, Long paymentid, String usercontent) {
        TOrder order=new TOrder();
        if(cartIds!=null && cartIds.length>0){
            int count=0;
            BigDecimal total=BigDecimal.ZERO;
            for(String cartId:cartIds){
                TCart cart=CartMapper.selectById(Long.parseLong(cartId));
                if(cart==null){
                    return null;
                }
                count+=cart.getCount();
                total =total.add(BigDecimal.valueOf(Double.valueOf(cart.getPrice())).multiply(BigDecimal.valueOf(cart.getCount())));
                TGoodSorder go=new TGoodSorder();
                go.setGoodsid(cart.getGoodsid());
                go.setOrderid(order.getId());
                go.setGoodsname(cart.getGoodsname());
                go.setStoreid(cart.getStoreid());
                go.setImg(cart.getImg());
                GoodsOrderMapper.insert(go);
                CartMapper.delete(new EntityWrapper<>(cart));
            }
            order.setOrdersn(RandomString.generateRandomString(8));
            order.setCreatedate(new Date());
            order.setStatus(1);
            order.setUserid(MemberUtils.getSessionLoginUser().getId());
            order.setUsername(MemberUtils.getSessionLoginUser().getUsername());
            order.setPaymentid(paymentid);
            order.setUsercontent(usercontent);
            order.setAddressid(addressid);
            //order.setOrderTotalPrice();
            OrderMapper.insert(order);

            TOrderLog log=new TOrderLog();
            log.setOrderId(order.getId());
            log.setOrderState("1");
            log.setStateInfo("提交订单");
            log.setCreateTime(new Date().getTime());
            log.setOperator(MemberUtils.getSessionLoginUser().getUsername());
            OrderLogMapper.insert(log);

            order.setTotalcount(count);
            order.setTotalprice(total);
            OrderMapper.updateById(order);
        }
        return order;

    }

    @Override
    public TOrder insertWapOrder(Long productId, Long addressid, Long paymentid, String usercontent, Long id, String username) {
        TOrder order=new TOrder();
        TGoods p =goodsMapper.selectById(productId);
        order.setOrdersn(RandomString.generateRandomString(8));
        order.setCreatedate(new Date());
        order.setStatus(1);
        order.setUserid(id);
        order.setUsername(username);
        order.setPaymentid(paymentid);
        order.setUsercontent(usercontent);
        order.setAddressid(addressid);
        order.setTotalcount(1);
        order.setTotalprice(BigDecimal.valueOf(Double.valueOf(p.getPrices())));
        OrderMapper.insert(order);

        TOrderLog log=new TOrderLog();
        log.setOrderId(order.getId());
        log.setOrderState("1");
        log.setStateInfo("提交订单");
        log.setCreateTime(new Date().getTime());
        log.setOperator(username);
        OrderLogMapper.insert(log);

        return order;
    }
}
