package com.stylefeng.guns.modular.shop.controller;

import com.stylefeng.guns.common.annotion.Permission;
import com.stylefeng.guns.common.annotion.log.BussinessLog;
import com.stylefeng.guns.common.constant.Const;
import com.stylefeng.guns.common.constant.Dict;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.controller.BaseController;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.shop.service.ITFloorService;
import com.stylefeng.guns.modular.shop.service.ITFloorService;
import com.stylefeng.guns.modular.system.warpper.DeptWarpper;
import com.stylefeng.guns.persistence.shop.model.TFloor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品类型表 前端控制器
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Controller
@RequestMapping("/tFloor")
public class TFloorController extends BaseController {
    private String PREFIX = "/shop/tFloor/";

    @Resource
    ITFloorService itFloorService;


    /**
     * 跳转到部门管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tFloor.html";
    }

    /**
     * 跳转到添加部门
     */
    @RequestMapping("/tFloor_add")
    public String tFloorAdd() {
        return PREFIX + "tFloor_add.html";
    }

    /**
     * 跳转到修改部门
     */
    @RequestMapping("/tFloor_update/{tFloorId}")
    public String tFloorUpdate(@PathVariable Integer tFloorId, Model model) {
        TFloor tFloor = itFloorService.selectById(tFloorId);
        model.addAttribute("tFloor",tFloor);
        LogObjectHolder.me().set(tFloor);
        return PREFIX + "tFloor_edit.html";
    }


    /**
     * 新增部门
     */
    @BussinessLog(value = "添加部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/add")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object add(TFloor tFloor) {
        return this.itFloorService.insert(tFloor);
    }

    /**
     * 获取所有部门列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        List<Map<String, Object>> list = this.itFloorService.selectMaps(null);
        return super.warpObject(new DeptWarpper(list));
    }

    /**
     * 部门详情
     */
    @RequestMapping(value = "/detail/{tFloorId}")
    @ResponseBody
    public Object detail(@PathVariable("tFloorId") Integer tFloorId) {
        return itFloorService.selectById(tFloorId);
    }

    /**
     * 修改部门
     */
    @BussinessLog(value = "修改部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/update")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object update(TFloor tFloor) {
        if (ToolUtil.isEmpty(tFloor) || tFloor.getId() == null) {
            throw new BussinessException(BizExceptionEnum.REQUEST_NULL);
        }
        itFloorService.updateById(tFloor);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除部门
     */
    @BussinessLog(value = "删除部门", key = "tFloorId", dict = Dict.DeleteDict)
    @RequestMapping(value = "/delete")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object delete(@RequestParam Long tFloorId) {

        itFloorService.deleteById(tFloorId);

        return SUCCESS_TIP;
    }
}
