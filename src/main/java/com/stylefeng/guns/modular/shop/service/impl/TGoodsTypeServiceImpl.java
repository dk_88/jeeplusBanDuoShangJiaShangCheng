package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.persistence.shop.model.TGoodsType;
import com.stylefeng.guns.persistence.shop.dao.TGoodsTypeMapper;
import com.stylefeng.guns.modular.shop.service.ITGoodsTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TGoodsTypeServiceImpl extends ServiceImpl<TGoodsTypeMapper, TGoodsType> implements ITGoodsTypeService {
	
}
