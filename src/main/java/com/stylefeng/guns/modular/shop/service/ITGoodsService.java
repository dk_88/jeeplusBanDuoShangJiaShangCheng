package com.stylefeng.guns.modular.shop.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.shop.model.TGoods;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITGoodsService extends IService<TGoods> {

    List<TGoods> selectProductByFloor(Long tid);

    List<TGoods> getProductByFloorid(Long tid);
    List<TGoods> getGoodsByBrandid(Long bid);

    Page<TGoods> selectgoodsListByType(int i, int i1, TGoods g);
}
