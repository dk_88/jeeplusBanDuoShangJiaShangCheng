package project.config.web.beetl;

import com.stylefeng.guns.core.beetl.GoodsFunction;
import com.stylefeng.guns.core.beetl.ProductClassFunctions;
import com.stylefeng.guns.core.beetl.ShiroExt;
import com.stylefeng.guns.core.util.ToolUtil;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;

public class BeetlConfiguration extends BeetlGroupUtilConfiguration {

	@Override
	public void initOther() {

		groupTemplate.registerFunctionPackage("shiro", new ShiroExt());
		groupTemplate.registerFunctionPackage("tool", new ToolUtil());
		groupTemplate.registerFunctionPackage("goods", new GoodsFunction());
		groupTemplate.registerFunctionPackage("productClass", new ProductClassFunctions());

	}

}
