/**
 * 初始化通知详情对话框
 */
var TGoodsInfoDlg = {
    tGoodsInfoData : {}
};

/**
 * 清除数据
 */
TGoodsInfoDlg.clearData = function() {
    this.tGoodsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TGoodsInfoDlg.set = function(key, val) {
    this.tGoodsInfoData[key] = (typeof value == "undefined") ? $("#" + key).val() : value;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TGoodsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TGoodsInfoDlg.close = function() {
    parent.layer.close(window.parent.TGoods.layerIndex);
}

/**
 * 收集数据
 */
TGoodsInfoDlg.collectData = function() {
    this.set('id').set('title').set('content');
}

/**
 * 提交添加
 */
TGoodsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tGoods/add", function(data){
        Feng.success("添加成功!");
        window.parent.TGoods.table.refresh();
        TGoodsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tGoodsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TGoodsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tGoods/update", function(data){
        Feng.success("修改成功!");
        window.parent.TGoods.table.refresh();
        TGoodsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tGoodsInfoData);
    ajax.start();
}

$(function() {

});
